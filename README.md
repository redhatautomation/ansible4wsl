# ansible4wsl

## Whats in the box?

Two playbooks:

- wsl.yml
- user.yml

### wsl.yml

Start off with envrionmental prep (using winrm for future work)

```yaml
---
- name: set up WSL
  hosts: '{{ target|default("localhost") }}'
  gather_facts: no
  vars:
    ansible_port: 5986
    ansible_user: "{{ win_user }}"
    ansible_password: "{{ win_password }}"
    ansible_become_password: "{{ admin_password }}"
    ansible_connection: "winrm"
    ansible_winrm_transport: "ntlm"
    ansible_winrm_server_cert_validation: "ignore"
    ansible_winrm_operation_timeout_sec: 120
    ansible_winrm_read_timeout_sec: 150
```

Checking to see if WSL is installed, and installing it if required

```yaml
  tasks:
    - name: check and install windows virtualization and WSL
      ansible.windows.win_optional_feature:
        name:
          - Microsoft-Windows-Subsystem-Linux
        state: present
```

You will need to reboot if WSL is installed this way. Currently that's manual but we will automate.

Next we download the package from microsoft, then extract the archive instead of using the .appx. If you were using a prepacked distro (wsl --export for instance) you could skip these steps and push that to the system. The reason we extract the archive here is to bypass the interactive prompt for a user and password at install

```yaml

    - name: get ubuntu 16.04 from microsoft
      ansible.windows.win_get_url: # requires ansible 2.10 or higher
        url: https://aka.ms/wsl-ubuntu-1604
 #       dest: "C:\\Users\\{{ user.name }}\\ubuntu_1604.appx"
        dest: C:\ubuntu_1604.zip

    - name: unpack WSL archive
      community.windows.win_unzip:
        src: c:\ubuntu_1604.zip
        dest: c:\ubuntu_1604 

    - name: create directory for WSL install
      ansible.builtin.win_file:
        path: c:\wsl\Ubuntu_1604
        state: directory
```

This is where we give the distro (either exported prepacked, or extracted from the one above) into wsl in order to be able to log in.

```yaml
- name: use WSL cli to import bundle
      ansible.builtin.win_command: wsl --import Ubuntu-16.04 c:\wsl\ubuntu_1604 c:\ubuntu_1604\install.tar.gz
```

### user.yml

As before, prepping the environment to work in windows.

```yaml
---
- name: set up WSL user
  hosts: '{{ target|default("localhost") }}'
  gather_facts: no
  vars:
    ansible_port: 5986
    ansible_user: "{{ win_user }}"
    ansible_password: "{{ win_password }}"
    ansible_become_password: "{{ admin_password }}"
    ansible_connection: "winrm"
    ansible_winrm_transport: "ntlm"
    ansible_winrm_server_cert_validation: "ignore"
    ansible_winrm_operation_timeout_sec: 120
    ansible_winrm_read_timeout_sec: 150
```

Since we didn't create a user at install, we do that now using user.yml For now we just create the user and password via commandline fed into the wsl distro

```yaml
    - name: create wsl user
      ansible.windows.win_shell: bash -c "adduser --home /home/demouser demouser"
      # ansible.windows.win_shell: bash -c "adduser --home /home/{{ wsl_user }} {{ wsl_user }}"

    - name: set wsl user password
      ansible.windows.win_shell: bash -c "echo demouser:chickentendies | chpasswd"
      # ansible.windwos.win_shell: bash -c "echo {{ wsl_user }}:{{ wsl_password }} | chpasswd"
```

Then we are taking our new user, and making them the default user, so that in subsequent launchers we login as "demouser"

```yaml
    - name: set default user via WSL filesystem
      ansible.windows.win_shell: bash -c "echo {{ item }} >> /etc/wsl.conf"
      with_items:
        - "[user]"
        - "default=demouser"
        # "default={{ wsl_user }}"
```

Last step is to terminate the wsl session so that our changes save

```yaml
    - name: stop WSL for changes to take effect
      ansible.windows.win_shell: wsl.exe -t Ubuntu-16.04
```
